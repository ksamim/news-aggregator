import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;


public class NAgg {
	public static void main(String[] args) throws InterruptedException
	{
		try {
        	Random rand = new Random(System.currentTimeMillis());
			BufferedReader rdr = new BufferedReader(new FileReader(args[0]));
			
			PrintWriter newFile = new PrintWriter(new BufferedWriter(new FileWriter(args[0].replaceAll("\\.txt", "")+"_out.txt",true)));
			
			int skip = 0;
			if(args.length>1)
			{
				skip = Integer.parseInt(args[1]);
			}
			else
			{
				newFile.close();
				newFile = new PrintWriter(new BufferedWriter(new FileWriter(args[0].replaceAll("\\.txt", "")+"_out.txt",false)));
			}

		    String line;
		    int count = 0;
		    while ((line = rdr.readLine()) != null)
		    {
		    	String stock = line;
		    	++count;
		    	if(count<skip)
		    	{
		    		continue;
		    	}
		    	if(count==skip)
		    	{
		    		//We found the stock we have to truncate at. Rewrite the file first
		    		BufferedReader copy_source = new BufferedReader(new FileReader(args[0].replaceAll("\\.txt", "")+"_out.txt"));
					PrintWriter copy_target = new PrintWriter(new BufferedWriter(new FileWriter(args[0].replaceAll("\\.txt", "")+"_temp.txt",false)));
					String copyLine;
					while((copyLine = copy_source.readLine()) != null)
					{
						if(!copyLine.startsWith(stock))
							copy_target.println(copyLine);
						else
							break;
					}
					copy_source.close();
					copy_target.close();
					FileUtils.copyFile(new File(args[0].replaceAll("\\.txt", "")+"_temp.txt"), new File(args[0].replaceAll("\\.txt", "")+"_out.txt"));

		    	}
	    		newFile.close();
				newFile = new PrintWriter(new BufferedWriter(new FileWriter(args[0].replaceAll("\\.txt", "")+"_out.txt",true)));
		    	
		    	System.out.println(stock+"\t"+count);
		    	URL article = new URL("http://finance.yahoo.com/rss/headline?s="+stock);
		    	URLConnection news = article.openConnection();
		    	BufferedReader yahoo = new BufferedReader(new InputStreamReader(news.getInputStream(), "UTF-8"));
		    	StringBuilder a = new StringBuilder();
		    	String rssFeed;
		    	while ((rssFeed = yahoo.readLine()) != null)
	                a.append(rssFeed);
	            yahoo.close();
	            String feed = a.toString();
	            
	            //Parse RSS
	            Pattern linkTag = Pattern.compile("<link>(.+?)</link>");
	            Matcher matcher = linkTag.matcher(feed);
	            ArrayList<String> links = new ArrayList<String>(); 
	            while(matcher.find()) {
	            	links.add(matcher.group(1));
	            }
	            
	            for(String link : links)
	            {
	            	if(link.contains("*")&&!link.contains("http://finance.yahoo.com/q/"))
	            	{
	            		String url = link.substring(link.lastIndexOf("*")+1).trim();
		            	Boolean retry = false;
		            	int abandon = 0;
		            	do {
		            		try {
			            		URL redirect = new URL(url);
			            		URLConnection follow = redirect.openConnection();
			            		follow.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)");
			            		BufferedReader source = new BufferedReader(new InputStreamReader(follow.getInputStream(), "UTF-8"));
			            		StringBuilder b = new StringBuilder();
			            		String info;
			            		while((info = source.readLine()) != null)
			            			b.append(info);
			            		source.close();
			            		String newArticle = b.toString();
			    	            
			    	            //Parse RSS
			    	            Pattern getBody = Pattern.compile("<body.*</body>");
			    	            Matcher bMatcher = getBody.matcher(newArticle);
			    	            if(bMatcher.find())
			    	            	newArticle = bMatcher.group(0).trim();
			    	            
			            		newArticle = newArticle.replaceAll("<style.*?</style>", " ");
			            		newArticle = newArticle.replaceAll("<script.*?</script>", " ");
			            		newArticle = newArticle.replaceAll("<.*?>", " ");
			            		newArticle = newArticle.replaceAll("[^ ]*[!@#$%^&*{}\\[\\]]+[^ ]*", "");
			            		newArticle = newArticle.replaceAll("[-.,()\\[\\]{}<>|\"';:?!@#$%\\^&*/_+=`~]", "");
			            		newArticle = newArticle.replaceAll("\\s+", " ");
			            		newArticle = newArticle.replaceAll("\\t","");
			            		newArticle = newArticle.replaceAll("\\n","");
			            		newArticle = newArticle.trim();
			            		
			            		if(newArticle.equals(""))
			            			continue;
			            		
			            		newFile.println(stock+"\t"+newArticle);
		            		}
			            	catch(IOException e) {
				            	long newRand = rand.nextInt(2521)+329;
				            	Pattern errorCode = Pattern.compile("[ ][0-9]+[ ]");
				            	Matcher findError = errorCode.matcher(e.getMessage().toString());
				            	if(findError.find())
				            		System.out.print("("+findError.group(0)+", "+(15000+newRand)+")");
				            	else
				            		System.out.print("!");
			            		mySleep(15000+newRand);
			            		retry = true;
			            		++abandon;
			            		if(abandon>5)
			            			retry=false;
			            	}
		            	} while(retry);
		            	System.out.print(".");
		            	long newRand = rand.nextInt(1000)+329;
	            		mySleep(5000+newRand);
	            	}
	            }
	            System.out.println();
		    }
		    newFile.close();
		    rdr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void mySleep(long millis) throws InterruptedException {
		long t0 = System.currentTimeMillis();
		  long millisLeft = millis;
		  while (millisLeft > 0) {
		    Thread.sleep(millisLeft);
		    long t1 = System.currentTimeMillis();
		    millisLeft = millis - (t1 - t0);
		  }
	}
}
