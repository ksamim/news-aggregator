import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class Split {
	public static void main(String[] args)
	{

		BufferedReader rdr;
		try {
			rdr = new BufferedReader(new FileReader("totalStocks.txt"));
			
			/************************/
		    int fileNum = 60;
			/************************/

		    String line;
		    int count = 0;
		    int totalCount = 6562;
		    int threshold = totalCount/fileNum;
		    int file = 1;
		    String newFile = "";
		    while ((line = rdr.readLine()) != null)
		    {
		    	++count;
		    	newFile+=line+"\n";
		    	if(count>threshold)
		    	{
		    		//Dump to file
		    		PrintWriter writer = new PrintWriter("page"+file+".txt", "UTF-8");
		    		writer.print(newFile);
		    		writer.close();
		    		newFile="";
		    		
		    		//Increment
		    		++file;
		    		count = 0;
		    	}
		    }
		    //Final file dump
    		//Dump to file
    		PrintWriter writer = new PrintWriter("page"+file+".txt", "UTF-8");
    		writer.print(newFile);
    		writer.close();
		    System.out.println(totalCount);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
